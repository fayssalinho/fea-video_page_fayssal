QUnit.module("SearchManager Tests", function () {
    const searchContainer = document.createElement('div');
    searchContainer.innerHTML = `
        <div class="form-group">
          <label for="quick-search">Search</label>
          <input type="text" class="form-control" id="quick-search" placeholder="Search ...">
        </div>
        <div class="form-group">
          <select id="selectPerPage" class="form-control">
            <option value="10">10</option>
            <option class="25">25</option>
            <option value="50">50</option>
          </select>
        </div>
        <div class="form-group">
          <label>
            <input id="moreTen" type="checkbox" value="">
            Users that have more than 10 likes
          </label>
        </div>
    `;
    const searchManager = new app.SearchManager({
        searchContainer,
        vimeo : app.vimeo
    });
    // const UIManagerPrototype = app.app.UIManager;
    
    // const videoContainer = document.createElement('div');
    // const videoTeplate = document.createElement('teplate');
    //     // TODO fil the teplate
    // const paginationContainer = document.createElement('nav');

    // const uiManager = new new app.UIManager({
    //     container: videoContainer,
    //     paginationContainer,
    //     videoTeplate,
    //     vimeo : app.vimeo
    // });

    QUnit.test('test _filterVideos with text', function (assert) {
        var results = searchManager._filterVideos({
            searchText : 'was filmed between 4th and 11th April 2011. I had'
        });
        var expectedResults = [{
            title : 'The Mountain'
        }]

        assert.equal(results[0].title, expectedResults[0].title, "The first's eleent title is ok");

        //TODO other results ( use assert.deepEquel )
    });

    QUnit.test('test _filterVideos with user have more than 3000 likes', function (assert) {
        var results = searchManager._filterVideos({
            moreThanTen : true
        });
        var expectedResults = [{
            title : 'A day in PARIS'
        }]

        assert.equal(results[0].title, expectedResults[0].title, "The first's element title is ok");
    });

    QUnit.test('test _filterVideos with  user more likes 3000 & text search... ', function (assert) {
        var results = searchManager._filterVideos({
            moreThanTen : true,
            searchText : 'Client_CELC Masters Of Linen'

        });
        var expectedResults = [{
            title : 'BE LINEN MOVIE'
        }]

        assert.equal(results[0].title, expectedResults[0].title, "The first's element title is ok");
    });

    QUnit.test('test _filterVideos with likes less than 3000 & search text ...', function (assert) {
        var results = searchManager._filterVideos({
            moreThanTen : false,
            searchText : 'my whole scene was ruined'

        });
        var expectedResults = [{
            title : 'The Mountain'
        }]

        assert.equal(results[0].title, expectedResults[0].title, "The first's element title is ok");
    });


});

// to show we can split tests by modules
QUnit.module("SearchManager Test callBacks", function () {
    const searchContainer = document.createElement('div');
    const searchManager = new app.SearchManager({
        searchContainer,
        vimeo : app.vimeo
    });


    // callback example (async like)
    QUnit.test('testing _callListeners with two callbacks', function(assert){
        var expectedResults = [{
            title : 'The Mountain'
        }];

        var done1 = assert.async(),
            done2 = assert.async();
        searchManager.onChange(({videos, itemsPerPage}) => {
            assert.equal(videos[0].title, expectedResults[0].title, "The 1st callback is called with the right title");
            done1();
        });
        searchManager.onChange(({videos, itemsPerPage}) => {
            assert.equal(videos[0].title, expectedResults[0].title, "The 2nd callback is caled with the right title");
            done2();
        });

        searchManager._callListeners({videos : expectedResults});
    });
});